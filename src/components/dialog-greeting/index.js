import React from "react";

export default (props) => {
	return (
		<span className="dialog-greeting">hello, {props.name}</span>
	);
}
