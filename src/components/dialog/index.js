import React from "react";

import DialogMain from "../dialog-main";

import "./index.scss";

export default (props) => {
	return (
		<div className="dialog">
			<DialogMain />
		</div>
	);
}
