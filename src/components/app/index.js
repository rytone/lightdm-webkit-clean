import React from "react";

import Dialog from "../dialog";

import "./index.scss";

export default class App extends React.Component {
	render() {
		return (
			<div className="app">
				<Dialog />
			</div>
		);
	}
}
