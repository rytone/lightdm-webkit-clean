import React from "react";

import DialogGreeting from "../dialog-greeting";
import DialogEntry from "../dialog-entry";

import "./index.scss";

export default (props) => {
	return (
		<div className="dialog-main">
			<DialogGreeting name="human" />
			<DialogEntry />
		</div>
	);
}
