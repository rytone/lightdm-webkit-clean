const path = require("path");
const ExtractTextPlugin = require("extract-text-webpack-plugin");

module.exports = {
	entry: "./src/index.js",

	output: {
		filename: "index.js",
		path: path.join(__dirname, "assets")
	},
	module: {
		rules: [
			{
				test: /\.(jpg|png|svg)/,
				loader: "file-loader",
				options: {
					name: "[name]-[hash].[ext]"
				}
			},
			{
				test: /\.scss$/,
				use: ExtractTextPlugin.extract({
					use: [
						{
							loader: "css-loader"
						},
						{
							loader: "sass-loader"
						}
					],
					fallback: "style-loader"
				})
			},
			{
				test: /\.(js|jsx)$/,
				loader: "babel-loader",
				exclude: /node_modules/,
				query: {
					presets: ["es2017", "react", "stage-0"],
				}
			}
		]
	},
	plugins: [
		new ExtractTextPlugin("index.css")
	]
};
